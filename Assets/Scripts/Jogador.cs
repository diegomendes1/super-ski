﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jogador : MonoBehaviour {
	public Jogo jogo;
	public float velocidade;
	public bool direita;

	public Sprite spriteEsquerda;
	public Sprite spriteDireita;
	public Sprite spriteAbatido;
	public SpriteRenderer spriteRenderer;

	void Update(){
		transform.Translate(Vector2.down*velocidade*2*Time.deltaTime);

		if(jogo.jogando){
			if(direita){
				transform.Translate(Vector2.right*velocidade*Time.deltaTime);
				spriteRenderer.sprite = spriteDireita;
			}else{
				transform.Translate(Vector2.left*velocidade*Time.deltaTime);
				spriteRenderer.sprite = spriteEsquerda;
			}
		
			VerificarCurva();
		}
	}

	void VerificarCurva(){
		if(Input.GetMouseButtonDown(0)){
			jogo.jogoAudio.TocarDirecao();
			
			if(direita)
				direita = false;
			else
				direita = true;
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		if(!col.gameObject.CompareTag("Arvore") && !col.gameObject.CompareTag("Barreira")){
			return;
		}

		if(jogo.jogando){
			jogo.FimJogo();
			spriteRenderer.sprite = spriteAbatido;
		}
	}
}
