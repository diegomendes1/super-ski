﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jogo : MonoBehaviour {
	public JogoUI ui;
	public JogoAudio jogoAudio;
	public bool jogando;
	public Transform posicaoInstancia;
	public ObjectPooler objectPooler;
	public Jogador jogador;

	[Header("Obstaculos")]
	float tempoAtual;
	public float intervaloTempo;
	float proximoTempo;

	[Header("Jogo")]
	public int pontos;
	int inicioContagem;
	int proximoNivel = 10;

	void Start(){
		jogando = false;
	}

	public void IniciarJogo(){
		jogando = true;
		InstanciarArvore();
		tempoAtual = Time.timeSinceLevelLoad;
		proximoTempo = tempoAtual + intervaloTempo;
		inicioContagem = (int)-jogador.transform.position.y;
	}

	public void InstanciarArvore(){
		GameObject novaArvore = objectPooler.SpawnFromPool("Arvore");
		novaArvore.transform.position = posicaoInstancia.position;
		novaArvore.transform.Translate(Random.Range(-2f, 2f), 0, 0);
		novaArvore.transform.rotation = Quaternion.identity;
	}
	
	void Update () {
		if(!jogando){
			return;
		}

		pontos = (int)-jogador.transform.position.y - inicioContagem;
		jogador.velocidade += Time.deltaTime/30;

		if(pontos >= proximoNivel){
			if(intervaloTempo > 0.1f){
				intervaloTempo -= 0.1f;
			}
			proximoNivel *= 10;
		}

		if(tempoAtual >= proximoTempo){
			InstanciarArvore();
			proximoTempo += intervaloTempo;
		}

		tempoAtual += Time.deltaTime;
	}

	public void FimJogo(){
		jogoAudio.TocarBatida();
		jogando = false;
		jogador.velocidade = 0;
		ui.FimJogo();

		if(PlayerPrefs.GetInt("Melhor") < pontos){
			PlayerPrefs.SetInt("Melhor", pontos);
			ui.melhor.SetActive(true);
		}
	}
}
