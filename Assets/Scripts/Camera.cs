﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {
	public Transform jogador;
	public int offset;

	void Update(){
		transform.position = new Vector3(transform.position.x, jogador.position.y + offset, transform.position.z);
	}
}
