﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class JogoUI : MonoBehaviour {
	
	public Jogo jogo;
	[Header("Painel Menu")]
	public GameObject painelMenu;
	public Text maioresPontos;

	[Header("Painel Jogo")]
	public GameObject painelJogo;
	public Text pontos;

	[Header("Painel Fim")]
	public GameObject painelFim;
	public Text pontosFinais;
	public GameObject melhor;

	void Update(){
		pontos.text = jogo.pontos.ToString();
	}

	void Start(){
		melhor.SetActive(false);
		painelMenu.SetActive(true);
		painelFim.SetActive(false);
		painelJogo.SetActive(false);

		if(!PlayerPrefs.HasKey("Melhor")){
			PlayerPrefs.SetInt("Melhor", 0);
		}
		
		maioresPontos.text = PlayerPrefs.GetInt("Melhor").ToString();
	}

	public void IniciarJogo(){
		painelMenu.SetActive(false);
		painelFim.SetActive(false);
		painelJogo.SetActive(true);
		jogo.IniciarJogo();
	}

	public void FimJogo(){
		painelFim.SetActive(true);
		painelJogo.SetActive(false);
		pontosFinais.text = pontos.text;
	}

	public void ReiniciarJogo(){
		SceneManager.LoadScene(0);
	}
}
