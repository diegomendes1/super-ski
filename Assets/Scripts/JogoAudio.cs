﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JogoAudio : MonoBehaviour {
	public AudioSource audioBatida;
	public AudioSource audioDirecao;
	public AudioSource audioMusica;

	void Start(){
		audioMusica.Play();
	}

	public void TocarBatida(){
		audioBatida.Play();
	}

	public void TocarDirecao(){
		audioDirecao.Play();
	}
}
