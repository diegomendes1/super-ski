﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColisorCima : MonoBehaviour {
	public Jogo jogo;
	public ObjectPooler objectPooler;
	//public Transform posicaoInstancia;


	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.CompareTag("Fundo") || col.gameObject.CompareTag("BarreiraControle")){
			GameObject novoFundo = objectPooler.SpawnFromPool(col.gameObject.tag);
			novoFundo.transform.position = jogo.posicaoInstancia.position;
			novoFundo.transform.rotation = Quaternion.identity;
		}
	}
}
